package at.htl.leonding.robolabcam.logic;

import org.opencv.core.*;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static org.opencv.imgcodecs.Imgcodecs.imread;


public class MotionController {
    private static MotionController ourInstance = new MotionController();

    public static MotionController getInstance() {
        return ourInstance;
    }

    CascadeClassifier faceCascade = new CascadeClassifier();
    private MotionController() {
        String path = MotionController.class.getClassLoader().getResource("cascades/haarcascade_frontalface_alt.xml").getPath();
        faceCascade.load(path.substring(1).replace('/','\\'));
    }

    private static int SENSITIVITY_VALUE=30;//30
    private static int BLUR_SIZE=15;

    Mat previousFrame;
    Mat currentFrame;

    Mat grayImage1;
    Mat grayImage2;

    Mat differenceImage;
    Mat thresholdImage;

    boolean inMovement;

    public void update(){

    }

    public void initMotionHistory(){

        currentFrame = new Mat();
        previousFrame = ImageProvider.getInstance().getImageAsMat();
        grayImage1 = new Mat();
        grayImage2 = new Mat();
        differenceImage = new Mat();
        thresholdImage = new Mat();

        Imgproc.cvtColor(previousFrame,grayImage1,Imgproc.COLOR_BGR2GRAY);
    }

    /**
     * http://www.robindavid.fr/opencv-tutorial/motion-detection-with-opencv.html
     * @return
     */
    public Mat updateMotionHistory(){
        currentFrame = ImageProvider.getInstance().getImageAsMat();

        Imgproc.cvtColor(currentFrame, grayImage2, Imgproc.COLOR_BGR2GRAY);

        Core.absdiff(grayImage1,grayImage2,differenceImage);
        Imgproc.threshold(differenceImage,thresholdImage,SENSITIVITY_VALUE,255,Imgproc.THRESH_BINARY);
        Imgproc.blur(thresholdImage,thresholdImage,new Size(BLUR_SIZE,BLUR_SIZE));
        Imgproc.threshold(thresholdImage,thresholdImage,SENSITIVITY_VALUE,  255,Imgproc.THRESH_BINARY);

        grayImage2.copyTo(grayImage1);
        searchForMovement(thresholdImage,currentFrame);
        //searchFace(thresholdImage,currentFrame);
        return currentFrame;
    }

    void searchForMovement(Mat thresholdImage, Mat frame){
        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(thresholdImage, contours, new Mat(), Imgproc.RETR_CCOMP, Imgproc.CHAIN_APPROX_SIMPLE);
        MovementManager.getInstance().startMovement(contoursInInterest(thresholdImage)? contours.size() : 0);
    }

    boolean contoursInInterest(Mat frame){
        Color colorTrigger = new Color(255, 255, 255);
        BufferedImage bimg = RoiImageProvider.getInstance().getBufferdImageFromMat(frame);
        for (int x = 0; x < bimg.getWidth(); x++){
            for (int y = 0; y < bimg.getHeight(); y++) {
                Color color = new Color(bimg.getRGB(x, y));
                if (color != null && color.equals(colorTrigger)) {
                    int posX = (int) (((double)x / (double)bimg.getWidth()) * (double)RoiImageProvider.getInstance().getWidth());
                    int posY = (int) (((double)y / (double)bimg.getHeight()) * (double)RoiImageProvider.getInstance().getHeight());
                    Color c = RoiImageProvider.getInstance().getRGBA(posX, posY);
                    if (c.getAlpha() == 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    void searchFace(Mat grayFrame, Mat frame){
        MatOfRect faces = new MatOfRect();

        String path = MotionController.class.getClassLoader().getResource("sims_stone01.png").getPath();
        Mat m = imread(path.substring(1).replace('/', '\\'),Imgcodecs.CV_LOAD_IMAGE_UNCHANGED);

        Imgproc.resize(m,m,new Size(365/4,750/4));

        //detect faces
        this.faceCascade.detectMultiScale(frame,faces);

//        imdecode(frame,Imgcodecs.CV_IMWRITE_PNG_STRATEGY);

        // each rectangle in faces is a face: draw them!
        Rect[] facesArray = faces.toArray();
        for (int i = 0; i < facesArray.length; i++){
            Imgproc.rectangle(frame, facesArray[i].tl(), facesArray[i].br(), new Scalar(0, 255, 0), 3);

//            break;
            Point tl = new Point(facesArray[i].x,facesArray[i].y);
            Point br = new Point(facesArray[i].x+m.width(),facesArray[i].y+m.height());

            Rect roi = new Rect(tl,br);
            Mat submat = frame.submat(m.rows(),m.rows()*2,m.cols(),m.cols()*2);

            m.copyTo(submat);
        }
    }

    public boolean isInMovement() {
        return inMovement;
    }
}
