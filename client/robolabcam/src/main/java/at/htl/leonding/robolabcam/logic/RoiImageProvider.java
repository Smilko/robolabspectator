package at.htl.leonding.robolabcam.logic;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgproc.Imgproc;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;

import static org.opencv.imgcodecs.Imgcodecs.imencode;

public class RoiImageProvider {

    private BufferedImage image;
    private LocalDateTime lastReadtime;
    private static RoiImageProvider instance = null;

    public static RoiImageProvider getInstance(){
        if(instance == null)
            instance = new RoiImageProvider();
        return instance;
    }

    private RoiImageProvider() {
        lastReadtime = LocalDateTime.MIN;
        loadImage();
    }

    private void loadImage(){
        try {
            image = ImageIO.read(new File(Properties.getProperty(Property.regionOfInteresFile)));
            //image = ImageIO.read(new File("test.png"));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    int maxX = 0, maxY = 0;
    public Color getRGBA(int x, int y){
        LocalDateTime now = LocalDateTime.now();
        //System.out.println(lastReadtime.compareTo(now));
        Color c = new Color(image.getRGB(x, y), true);
        //System.out.println(x);
        //System.out.println(y);
        return c;
    }

    public int getWidth(){
        return image.getWidth();
    }

    public int getHeight(){
        return image.getHeight();
    }

    //For Debugging
    /*public void saveImage(Mat image){
        BufferedImage bimg = getBufferdImageFromMat(image);
        File outputfile = new File("test.png");
        try {
            ImageIO.write(bimg, Properties.getProperty(Property.fileFormat), outputfile);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    public BufferedImage getBufferdImageFromMat(Mat image){
        Mat frame = new Mat();
        image.copyTo(frame);
        //Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2RGB);
        MatOfByte buffer = new MatOfByte();
        imencode(".png", frame, buffer);
        BufferedImage bimg = null;
        try {
            bimg = ImageIO.read(new ByteArrayInputStream(buffer.toArray()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        buffer.release();
        frame.release();
        return bimg;
    }
}
