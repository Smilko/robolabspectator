package at.htl.leonding.robolabcam.logic;

import org.opencv.videoio.VideoCapture;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import static org.opencv.videoio.Videoio.CV_CAP_PROP_FRAME_HEIGHT;
import static org.opencv.videoio.Videoio.CV_CAP_PROP_FRAME_WIDTH;

public class CameraProvider {

    private static final int CP_CAM_ID = 0;

    private static CameraProvider ourInstance = new CameraProvider();

    public static CameraProvider getInstance() {
        return ourInstance;
    }

    private VideoCapture videoCapture;

    private CameraProvider() {
        setup();
    }

    private void setup() {
        videoCapture = new VideoCapture(CP_CAM_ID);

        if (!videoCapture.isOpened()) {
            System.out.println("Unable to use Cam: "+CP_CAM_ID);
        }
    }

    public int getVideoWidth() {
        if (videoCapture.isOpened()) {
            return (int) videoCapture.get(CV_CAP_PROP_FRAME_WIDTH);
        }
        return 0;
    }

    public int getVideoHeight() {
        if (videoCapture.isOpened()) {
            return (int) videoCapture.get(CV_CAP_PROP_FRAME_HEIGHT);
        }
        return 0;
    }

    public VideoCapture getVideoCapture() {
        if(videoCapture == null || !videoCapture.isOpened()){
            setup();
        }
        if(!videoCapture.isOpened()){
            throw new NotImplementedException();
        }
        return videoCapture;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        videoCapture.release();
    }
}
