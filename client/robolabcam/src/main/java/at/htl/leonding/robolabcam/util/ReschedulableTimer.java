package at.htl.leonding.robolabcam.util;

import java.util.Timer;
import java.util.TimerTask;

public class ReschedulableTimer extends Timer {
    private Runnable task;
    private TimerTask timerTask;

    public void schedule(Runnable runnable, long delay) {
        task = runnable;
        timerTask = new TimerTask() {
            public void run() {
                task.run();
            }
        };

        this.schedule(timerTask, delay);
    }

    public void reschedule(long delay) {
        if (timerTask != null) {
            timerTask.cancel();
            timerTask = new TimerTask() {
                public void run() {
                    task.run();
                }
            };
            this.schedule(timerTask, delay);
        }
    }

    public void stop() {
        if (timerTask != null)
            timerTask.cancel();
    }
}