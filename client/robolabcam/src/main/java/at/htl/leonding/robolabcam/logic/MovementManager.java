package at.htl.leonding.robolabcam.logic;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;

import java.io.IOException;
import java.util.*;

public class MovementManager {
    private static MovementManager ourInstance = new MovementManager();
    public static MovementManager getInstance() {
        return ourInstance;
    }
    private Date lastTime;
    private int cntNoMovement;
    private int cntMovement;

    private MovementManager() {
        lastTime = new Date();
    }

    public void startMovement(int contours) {
        try {
            Date duration = new Date(new Date().getTime() - lastTime.getTime());
            if(duration.getSeconds() >= Properties.getPropertyDouble(Property.screenDuration))
            {
                double ratio = 0.0;
                ratio = (double) cntMovement / (double)(cntMovement + cntNoMovement);
                cntMovement = 0;
                cntNoMovement = 0;
                if (ratio >= Properties.getPropertyDouble(Property.movementRatio)){
                    if(duration.getSeconds() >= Properties.getPropertyDouble(Property.groupAfterTime))
                        ScreenshotController.getInstance().incrementGrouping();
                    ScreenshotController.getInstance().saveScreenshot();
                    lastTime = new Date();
                }
            }
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(contours == 0)
            cntNoMovement++;
        else
            cntMovement++;
    }
}
