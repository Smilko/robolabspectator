package at.htl.leonding.robolabcam.logic;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgproc.Imgproc;
import processing.core.PConstants;
import processing.core.PImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.opencv.core.CvType.CV_8UC4;
import static org.opencv.imgcodecs.Imgcodecs.imencode;

public class ImageProvider {
    private static ImageProvider ourInstance = new ImageProvider();

    public static ImageProvider getInstance() {
        return ourInstance;
    }

    private ImageProvider() {
    }

    private void setup(){

    }

    public BufferedImage getImage() {
        Mat frame = getImageAsMat();

        //Imgproc.cvtColor(frame, frame, Imgproc.COLOR_BGR2RGB);

        MatOfByte buffer = new MatOfByte();

        imencode(".jpg", frame, buffer);

        BufferedImage bimg = null;
        try {
            bimg = ImageIO.read(new ByteArrayInputStream(buffer.toArray()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        buffer.release();
        frame.release();
        return bimg;
    }

    public static PImage MatToPImage(Mat mat) {
        if (mat != null && !mat.empty()) {
            MatOfByte matOfByte = new MatOfByte();
            imencode(".jpg", mat, matOfByte);
            byte[] byteArray = matOfByte.toArray();
            try {
                InputStream in = new ByteArrayInputStream(byteArray);
                BufferedImage bimg = ImageIO.read(in);
                PImage img = new PImage(bimg.getWidth(), bimg.getHeight(), PConstants.ARGB);
                bimg.getRGB(0, 0, img.width, img.height, img.pixels, 0, img.width);
                img.updatePixels();
                bimg = null;
                in.close();

                return img;
            } catch (Exception e) {
                e.printStackTrace();
            }
            matOfByte.release();
        }

        return null;
    }

    public Mat getImageAsMat(){
        Mat frame = new Mat();

        CameraProvider.getInstance().getVideoCapture().read(frame);
        Mat rgba = new Mat(frame.size(), CV_8UC4);
        Imgproc.cvtColor(frame,rgba, Imgproc.COLOR_BGR2BGRA,4);
        frame.release();
        return rgba;
    }
}
