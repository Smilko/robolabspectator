package at.htl.leonding.robolabcam;

import at.htl.leonding.robolabcam.logic.*;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;
import processing.core.PApplet;
import processing.core.PImage;

public class Main  {

    public static void main(String args[]) {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        //VideoCapture v = new VideoCapture();
        setup();
        System.out.println("Starting to update images ...");
        while (true)
            update();
    }

    public static void setup() {
        System.out.println("Setting up ...");
        MotionController.getInstance().initMotionHistory();
        MovementManager.getInstance();
    }

    public static void update() {
        MotionController.getInstance().updateMotionHistory();
    }
}
