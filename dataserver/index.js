/**
 * Robolabcam - REST for the webapp
 * Author: Andreas Schlömicher
 * HTL Leonding @2017
 */
let http = require('http');
var fs = require('fs');
const path = require('path');
const express = require('express');
var cors = require('cors');
var os = require('os');
var bodyParser = require('body-parser');
var getSize = require('get-folder-size');
const diskusage = require('diskusage');
const rimraf = require('rimraf');


var screenDuration = -1;
var movementRatio = -1.0;
var groupAfterTime = -1;
var regionImageFileName;

const app = express();
const port = 3021;
const server = http.createServer(app);

const FTP_TYPE_FILE = 0;
const FTP_TYPE_DIRECTORY = 1;

const FTP_DIRECTORY = process.argv[2];
const IP_ADDRESS = process.argv[3];
const APPICATION_PATH = process.argv[4];
const PROPERTY_FILE = process.argv[5];
const IP_CAM = process.argv[6];
const CONFIG_PATH = process.argv[7];
const INFODATPATH = path.join(CONFIG_PATH, "info.dat");
const REGION_FILE_PATH = path.join(CONFIG_PATH, "region.png");

if (FTP_DIRECTORY == undefined) {
    console.error('No FTP Root directory parameter found')
    process.exit(1)
}


console.log('---------------------------------------------------');
console.log('--- Robolabcam - REST for Webapp                ---');
console.log();
console.log(`--- FTP Root directory: ${FTP_DIRECTORY}`);
console.log(`--- IP: ${IP_ADDRESS}`);
console.log(`--- Propertyfile: ${PROPERTY_FILE}`);
console.log(`--- IP CAM: ${IP_CAM}`);
console.log('---------------------------------------------------');

app.use(cors());
app.use(bodyParser.urlencoded({limit: '50mb'}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.text({limit: '50mb'}));

refreshConfigVariables();

function getAllRootDirectorys() {
    return fs.readdirSync(FTP_DIRECTORY).filter(f => fs.statSync(path.join(FTP_DIRECTORY, f)).isDirectory());
}

function getAllFilesOfDirectory(directory) {
    return fs.readdirSync(directory).filter(f => fs.statSync(path.join(directory, f)).isFile());
}

function refreshConfigVariables() {
    var stream = fs.createReadStream(path.join(APPICATION_PATH, PROPERTY_FILE));
    stream.on('readable', function () {
        var buffer = stream.read();
        if (buffer) {
            var lines = buffer.toString().split('\n');
            var cnt = 0;
            while (cnt < lines.length) {
                console.log(lines[cnt]);
                var actLine = lines[cnt].split('=');
                if (actLine[0] === 'screenDuration') {
                    screenDuration = actLine[1];
                }
                if (actLine[0] === 'movementRatio') {
                    movementRatio = actLine[1];
                }
                if (actLine[0] === 'groupAfterTime') {
                    groupAfterTime = actLine[1];
                }
                if (actLine[0] === 'regionImageFileName') {
                    regionImageFileName = actLine[1];
                }
                cnt++;
            }
        }
    });
}

function updateConfiguration() {
    var stream = fs.createReadStream(path.join(APPICATION_PATH, PROPERTY_FILE));
    stream.on('readable', function () {
        var buffer = stream.read();
        if (buffer) {
            var newFile = '';
            var lines = buffer.toString().split('\n');
            var cnt = 0;
            while (cnt < lines.length) {
                if (cnt != 0) {
                    newFile += '\n';
                }

                var actLine = lines[cnt].split('=');
                if (actLine[0] === 'screenDuration') {
                    newFile += 'screenDuration=' + screenDuration;
                } else if (actLine[0] === 'movementRatio') {
                    newFile += 'movementRatio=' + movementRatio;
                } else if (actLine[0] === 'groupAfterTime') {
                    newFile += 'groupAfterTime=' + groupAfterTime;
                } else {
                    newFile += lines[cnt];
                }
                cnt++;
            }
            writeStream = fs.createWriteStream(path.join(APPICATION_PATH, PROPERTY_FILE));
            writeStream.write(newFile);
        }
    });
}

app.get('/setting/refresh', (request, response) => {
    refreshConfigVariables();
    response.send('Refreshing config ...');
});
app.get('/setting/screenDuration', (request, response) => {
    response.send(screenDuration.toString());
});
app.put('/setting/screenDuration/:screenValue', (request, response) => {
    screenDuration = request.params.screenValue;
    updateConfiguration();
    response.send(200);
});
app.get('/setting/movementRatio', (request, response) => {
    response.send(movementRatio.toString());
});
app.put('/setting/movementRatio/:movementValue', (request, response) => {
    screenDuration = request.params.movementValue;
    updateConfiguration();
    response.send(200);
});
app.get('/setting/groupAfterTime', (request, response) => {
    response.send(groupAfterTime.toString());
});
app.put('/setting/groupAfterTime/:groupValue', (request, response) => {
    groupAfterTime = request.params.groupValue;
    updateConfiguration();
    response.send(200);
});
app.post('/setting/screenArea', (request, response) => {
    fs.writeFile(REGION_FILE_PATH, request.body.imageData, {encoding: 'base64'}, function (err) {
        if (err) throw err;
        console.log('Screenarea saved.');
        response.sendStatus(200);
    });
});

app.get('/setting/screenArea', (request, response) => {
    let rstream = fs.createReadStream(REGION_FILE_PATH);
    rstream.pipe(response);
});

/**
 * Return all movements in history (all directorys)
 */
app.get('/history', (request, response) => {
    let output = [];
    let dateFrom = new Date(request.query.from);
    let dateTo = new Date(request.query.to);

    getAllRootDirectorys().forEach(directory => {
        let images = getAllFilesOfDirectory(FTP_DIRECTORY + directory);
        output.push(({
            folder: directory,
            images: images
        }));
    });


    output.sort(function (a, b) {
        var keyA = new Date(a.folder),
            keyB = new Date(b.folder);
        // Compare the 2 dates
        if (keyA < keyB) return 1;
        if (keyA > keyB) return -1;
        return 0;
    })

    let result = output;
    if (!isNaN(dateFrom) && !isNaN(dateTo)) {
        result = [];
        output.forEach(item => {
            if (new Date(item.folder) >= dateFrom
                && new Date(item.folder) <= dateTo) {
                result.push(item);
            }
        });
    }

    response.send(result);
});


/**
 * Return all movements in history (all directorys)
 */
app.get('/history/latest', (request, response) => {
    let output = [];
    getAllRootDirectorys().forEach(directory => {
        output.push(({
            folder: directory,
        }));
    });

    output.sort(function (a, b) {
        var keyA = new Date(a.folder),
            keyB = new Date(b.folder);
        // Compare the 2 dates
        if (keyA < keyB) return 1;
        if (keyA > keyB) return -1;
        return 0;
    })
    response.send(output.slice(0, Math.min(3, output.length)));
});

/**
 * Return Images
 */
app.get('/image/:folder/:file', (req, res) => {

    let imagePath = '/' + req.params.folder + '/' + req.params.file;

    let readPath = FTP_DIRECTORY + imagePath;

    var rstream = fs.createReadStream(readPath);
    rstream.pipe(res);
});

/**
 * Upload Image
 */
app.post('/image/:folder/:filename', (req, res) => {

    let imageDirectory = path.join(FTP_DIRECTORY, req.params.folder);

    let writePath = path.join(imageDirectory, req.params.filename);

    if (!fs.existsSync(imageDirectory)) {
        fs.mkdirSync(imageDirectory);
    }

    getSize(FTP_DIRECTORY, function (err, size) {
        if (err) {
            throw err;
        }

        let percentage = 0;

        let diskPath = './data';

        try {
            let info = diskusage.checkSync(diskPath);
            percentage = (size / info.total * 100).toFixed(0);

            if (percentage >= 95) {
                let dirs = getAllRootDirectorys().sort(function (a, b) {
                    let keyA = new Date(a.folder),
                        keyB = new Date(b.folder);
                    // Compare the 2 dates
                    if (keyA < keyB) return 1;
                    if (keyA > keyB) return -1;
                    return 0;
                });
                rimraf(path.join('data', dirs[0]), function () {
                    console.log('Disk space almost consumed! -> Deleted oldest Movement');
                });
            }
        }
        catch (err) {
            console.log(err);
        }
    });
    fs.writeFile(writePath, req.body, 'base64', function (err) {
        if (err) throw err;
        console.log(`Saved ${req.params.filename} in ${req.params.folder}`);
        res.sendStatus(200);
    });

});

app.get('/settingFile', (req, res) => {
    var file = fs.readFileSync(PROPERTY_FILE, "utf8");
    res.send(file);
});

app.get('/infoDat', (req, res) => {
    var file = fs.readFileSync(INFODATPATH, "utf8");
    res.send(file);
});

app.get('/text', (req, res) => {
    var rstream = fs.createReadStream(INFODATPATH);
    rstream.on('error', function () {
        res.send('');
    });
    rstream.pipe(res);
});


app.post('/text', (req, res) => {
    fs.writeFileSync(INFODATPATH, req.body, 'utf8');
    res.send(req.body);
});

app.get('/', (req, res) => {
    fs.readFile('index.html', "utf8", function (err, html) {
        if (err) throw err;

        let movements = getAllRootDirectorys().length;
        html = html.replace("{ MOVEMENTS }", movements);

        getSize(FTP_DIRECTORY, function (err, size) {
            if (err) {
                throw err;
            }

            let percentage = 0;

            let diskPath = './data';

            try {
                let info = diskusage.checkSync(diskPath);
                // console.log(info.available);
                // console.log(info.free);
                // console.log(info.total);
                // console.log(size);
                percentage = (size / info.total * 100).toFixed(0);
            }
            catch (err) {
                console.log(err);
            }


            if (size > 1000 * 1000 * 1000) { //GB
                html = html.replace("{ SIZE }", (size / 1000 / 1000 / 1000).toFixed(2));
                html = html.replace("{ SIZE_UNIT }", "gB");
            } else if (size > 1000 * 1000) { //MB
                html = html.replace("{ SIZE }", (size / 1000 / 1000).toFixed(2));
                html = html.replace("{ SIZE_UNIT }", "mB");
            } else if (size > 1000) { //KB
                html = html.replace("{ SIZE }", (size / 1000).toFixed(2));
                html = html.replace("{ SIZE_UNIT }", "kB");
            }
            else {
                html = html.replace("{ SIZE }", size);
                html = html.replace("{ SIZE_UNIT }", "Bytes");
            }

            if (percentage >= 80) {
                html = html.replace("{ SIZE_COLOR }", "danger");
            } else if (percentage >= 50) {
                html = html.replace("{ SIZE_COLOR }", "warning");
            } else {
                html = html.replace("{ SIZE_COLOR }", "success");
            }

            //Percentage
            html = html.replace("{ SIZE_PERCENTAGE }", percentage);

            res.end(html);
        });

    });
});

app.get('/bootstrap', (req, res) => {
    var rstream = fs.createReadStream("node_modules/bootstrap/dist/css/bootstrap.min.css");
    rstream.on('error', function () {
        res.send(404);
    });
    rstream.pipe(res);
});

app.get('/cam', (req, res) => {
    res.redirect(IP_CAM);
});

let ifaces = os.networkInterfaces();

let lastIP = null;

Object.keys(ifaces).forEach(function (ifname) {

    ifaces[ifname].forEach(function (iface) {
        if ('IPv4' !== iface.family || iface.internal !== false) {
            // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
            return;
        }

        lastIP = iface.address;
    });
});

if (IP_ADDRESS == null) {
    lastIP = IP_ADDRESS;
}

/**
 * Open Server
 */
//server.listen(port,'127.0.0.1')
server.listen(port, IP_ADDRESS, (err) => {
    if (err) {
        return console.log(`Error: ${err}`)
    }

    console.log(`Server is listening on ${lastIP}:${port}`)
});
