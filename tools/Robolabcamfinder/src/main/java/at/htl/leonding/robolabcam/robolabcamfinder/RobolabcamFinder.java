package at.htl.leonding.robolabcam.robolabcamfinder;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class RobolabcamFinder {
    public static void main(String[] args) throws IOException, URISyntaxException {

        String Network = "172.18.252.";

        String searchedMacAdress = "b8:27:eb:d5:5a:2c";
        int port = 9000;

        for (int i = 1; i < 255; i++) {
            String[] pingCommand = {"ping", "-c","2", "-w" ,"20", Network + i};
            executeCommand(pingCommand);

            String[] commands = {"arp", "-a", Network + i};

            List<String> result = executeCommand(commands);

            if (result.size() >= 3) {
                String[] line = result.get(3).trim().replaceAll(" +", " ").split(" ");
                if (line.length == 3) { // Result is ok
                    System.out.println(String.format("%s| %s", line[0], line[1]));
                    if(line[1].equals(searchedMacAdress)){
                        System.out.println("Device found - open in browser");
                        String url = String.format("http://%s:%d",line[0],port);
                        if (Desktop.isDesktopSupported()) {
                            Desktop.getDesktop().browse(new URI(url));
                            return;
                        }
                    }
                }
            }
            else {
                for (String item: result) {
                    System.out.println(Network + i+"| "+item);
                }
            }
        }

    }

    private static List<String> executeCommand(String[] commands) throws IOException {
        Runtime rt = Runtime.getRuntime();
        Process proc = rt.exec(commands);

        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));

        List<String> result = new ArrayList<String>();

        // read the output from the command
        String s = null;
        while ((s = stdInput.readLine()) != null) {
            result.add(s);
        }

        // read any errors from the attempted command
        while ((s = stdError.readLine()) != null) {
            result.add(s);
            System.out.println(s);
        }

        return result;
    }
}
