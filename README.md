![Robolabcam Image](./webapp/app/icon.png)
# Robolabcam

## Systemarchitektur
![Robolabcam Image](Systemarchitektur.png)

# Ausgangsstellung
Das Robolab an der Schule. Jährlich schrumpft der Bestand an Schuleigentum wie zum Beispiel Raspberry Pi's, LAN-Kabel und Router

# Aufgabestellung
Eine Überwachungskamera im Robolab um Diebstähle aufzuklären bzw. die Diebe abzuschrecken.

# Ziele
- Zukünftige Diebstähle aus dem Robolab werden verhindert
- Der Lehrer kann die Arbeitsmoral der Schüler überwachen

# Anforderungen
- Livestreams
- Bildgalerien für Bewegungsmomente

# Nicht funktionale Anforderungen
- Das System muss auch nach Stromausfällen den Normalzustand wiederherstellen
- Das System muss sich den Speicherplatz für die Fotos richtig einteilen

# Verwendete eigene Repositories
- [Motion (forked)](https://github.com/Schloemicher/motion)

## Bestandteile

### REST APP (NodeJs)
Die REST APP verwaltet die Bewegungsgallerien, Einstellungen und Info Texte.

**Dependencies**
- diskusage
- rimraf
- bootstrap
- get-folder-size
- body-parser
- cors

### Java App
Die Java App erkennt Bewegungen im Livestream und speichert diese auf dem REST Service ab oder sendet die Bilder an einen MQTT Broker.

**Dependencies**
- [Webcam Capture API](http://webcam-capture.sarxos.pl/)
- [Paho Mqtt Client](https://www.eclipse.org/paho/)

### Webapp NodeJs
Die Web Oberfläche ist in nativ Javascript implementiert.

**Dependencies**
- gulp
- browser-sync
- babel
- bootstrap