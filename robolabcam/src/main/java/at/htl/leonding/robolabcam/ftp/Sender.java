package at.htl.leonding.robolabcam.ftp;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPCmd;
import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static javax.print.attribute.standard.ReferenceUriSchemesSupported.FTP;

public class Sender {
    private static Sender instance = new Sender();

    private Sender() {
    }

    public static Sender getInstance() {
        return instance;
    }

    public boolean chechAndCreteFolder(String pathToFolder) {
        return false;
    }

    public boolean sendFile(File file, String destinationFilePath, boolean fileInString) {
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.connect(Properties.getProperty(Property.ftpServerAdress), Properties.getPropertyInt(Property.ftpPort));
            ftpClient.login(Properties.getProperty(Property.ftpUser), Properties.getProperty(Property.ftpPassword));
            makeDirectoriesIfNotExists(ftpClient, destinationFilePath, fileInString);

            ftpClient.sendCommand(FTPCmd.PASSIVE);
            ftpClient.setFileType(org.apache.commons.net.ftp.FTP.BINARY_FILE_TYPE);
            InputStream inputStream = new FileInputStream(file);

            goToPath(ftpClient, destinationFilePath, fileInString);
            String[] dirs = destinationFilePath.split(File.separator);
            ftpClient.storeFile(dirs[dirs.length - 1], inputStream);
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (ftpClient.isConnected()) {
                    ftpClient.logout();
                    ftpClient.disconnect();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return true;
    }

    private void goToPath(FTPClient ftpClient, String filePath, boolean fileInString) throws IOException {
        String[] directories = filePath.split(File.separator);
        int max = fileInString ? directories.length - 1 : directories.length;
        int cnt = 0;

        while (cnt < max) {
            ftpClient.changeWorkingDirectory(directories[cnt]);
            cnt++;
        }

    }

    private void makeDirectoriesIfNotExists(FTPClient ftpClient, String path, boolean fileInString) throws IOException {
        String[] directories = path.split(File.separator);
        int max = fileInString ? directories.length - 1 : directories.length;
        int cnt = 0;

        while (cnt < max) {
            String parentPath = createParentPath(directories, cnt);
            if (!containsPath(ftpClient.listDirectories(parentPath), directories[cnt])) {
                ftpClient.changeWorkingDirectory(parentPath);
                ftpClient.makeDirectory(directories[cnt]);
            }
            cnt++;
        }
    }

    private boolean containsPath(FTPFile[] returnedDirectories, String path) {
        for (FTPFile file : returnedDirectories) {
            if (file.isDirectory() && file.getName().equals(path))
                return true;
        }
        return false;
    }

    private String createParentPath(String[] directories, int untilIndex) {
        StringBuilder stringBuilder = new StringBuilder();
        int cnt = 0;
        String finalPath = null;

        while (cnt < untilIndex) {
            stringBuilder.append(File.separator);
            stringBuilder.append(directories[cnt]);
            cnt++;
        }
        finalPath = stringBuilder.toString();
        if (finalPath.equals(""))
            return File.separator;
        return finalPath;
    }
}
