package at.htl.leonding.robolabcam;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;
import at.htl.leonding.robolabcam.logic.MotionController;
import at.htl.leonding.robolabcam.rest.Sync;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, NoSuchFieldException {
        System.out.println("----------------------------------------------");
        System.out.println("--- ROBOLABCAM Movement and Controller App ---");
        System.out.println("----------------------------------------------");

        System.out.println();
        System.out.println("REST URL: " + Properties.getProperty(Property.serverURL));
        System.out.println("MQTT URL: " + Properties.getProperty(Property.mqttURL));
        System.out.println();
        setup();


        System.out.println("Starting to update images ...");
        System.in.read();
    }

    public static void setup() {
//        MovementManager.getInstance();
        MotionController.getInstance();
        Sync.getInstance().init();
    }
}
