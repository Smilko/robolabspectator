package at.htl.leonding.robolabcam.config;

import java.io.*;

public class Properties {

    private static java.util.Properties prop  = null;
    private static InputStream input = null;

    public static boolean fileExists(){
        return new File(Property.fileName).exists();
    }

    public static void rewriteConfigFile(String newText){
        try {
            FileOutputStream stream = new FileOutputStream(Property.fileName);
            stream.write(newText.getBytes());
            stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void openFile(){
        prop = new java.util.Properties();
        try {
            input = new FileInputStream(Property.fileName);
        } catch (FileNotFoundException e) {
            System.err.println("File could not be found!\n"+ e);
        }
    }

    private static void closeFile(){
        try {
            input.close();
        } catch (IOException e) {
            System.err.println("Error closing file: " + e);
        }
    }

    public static String getProperty(String property) throws NoSuchFieldException, IOException {
        openFile();
        prop.load(input);
        if(!prop.containsKey(property))
        {
            closeFile();
            throw new NoSuchFieldException("Property \"" + property + "\" not found!");
        }
        closeFile();
        return prop.getProperty(property);
    }

    public static int getPropertyInt(String property) throws NoSuchFieldException, IOException {
        return Integer.parseInt(getProperty(property));
    }

    public static long getPropertyLong(String propterty) throws NoSuchFieldException, IOException {
        return Long.parseLong(getProperty(propterty));
    }

    public static double getPropertyDouble(String property) throws NoSuchFieldException, IOException {
        return Double.parseDouble(getProperty(property));
    }

    public static boolean getPropertyBoolean(String property) throws NoSuchFieldException, IOException {
        return Boolean.parseBoolean(getProperty(property));
    }
}
