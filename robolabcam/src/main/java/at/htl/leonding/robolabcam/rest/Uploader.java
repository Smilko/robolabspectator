package at.htl.leonding.robolabcam.rest;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Uploader {
    public static void sendImage(BufferedImage bi, String filename) throws NoSuchFieldException, IOException {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        ImageIO.write(bi, Properties.getProperty(Property.fileFormat), stream);
        stream.flush();
        byte[] encoded = java.util.Base64.getEncoder().encode(stream.toByteArray());
        String data = new String(encoded);

        if(Properties.getPropertyBoolean(Property.useMqttUpload)){
            publishOnMqtt(data);
        }
        if(Properties.getPropertyBoolean(Property.useRestUpload)){
            send(new StringEntity(data), filename);
        }
    }

    private static void send(StringEntity body, String path) throws NoSuchFieldException, IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        String url = String.format("%s/image/%s", Properties.getProperty(Property.serverURL), path);
        HttpPost request = new HttpPost(url);

        //request.setHeader("Content-Type", "application/json");
        request.setEntity(body);

        HttpResponse response = httpclient.execute(request);
        System.out.println(path +
                " uploaded: " +
                response.getStatusLine().getStatusCode() +
                " " +
                response.getStatusLine().getReasonPhrase());
        request.releaseConnection();
    }

    private static void publishOnMqtt(String data) {
        MqttClient client = null;
        try {
            client = new MqttClient(Properties.getProperty(Property.mqttURL), MqttClient.generateClientId());
            client.connect();
            MqttMessage message = new MqttMessage();
            message.setPayload(data.getBytes());
            client.publish(Properties.getProperty(Property.mqttChannel), message);
            client.disconnect();

            System.out.println("Image sent to Mqtt Broker");
        } catch (MqttException | IOException | NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

}
