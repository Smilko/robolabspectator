package at.htl.leonding.robolabcam.logic;


import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamMotionDetector;
import com.github.sarxos.webcam.WebcamMotionEvent;
import com.github.sarxos.webcam.WebcamMotionListener;

import java.awt.*;
import java.util.ArrayList;


public class MotionController implements WebcamMotionListener {
    private static MotionController ourInstance = new MotionController();

    private WebcamMotionDetector detector;

    public static MotionController getInstance() {
        return ourInstance;
    }

    private MotionController() {
        initMotionHistory();
    }

    public void initMotionHistory() {
        detector = new WebcamMotionDetector(CameraProvider.getInstance().getWebcam());
        detector.setInterval(100); // one check per 100 ms
        detector.addMotionListener(this);
        detector.start();
    }

    @Override
    public void motionDetected(WebcamMotionEvent webcamMotionEvent) {
        boolean takeScreen = motionInRoi(webcamMotionEvent.getPoints(),
                webcamMotionEvent.getCurrentImage().getWidth(),
                webcamMotionEvent.getCurrentImage().getHeight());
        MovementManager.getInstance().startMovement(takeScreen ? 1 : 0);
    }

    private boolean motionInRoi(ArrayList<Point> points, int width, int height){
        try{
            RoiImageProvider r = RoiImageProvider.getInstance();
            for (Point p: points) {
                int x = (int)((p.getX() / width)*r.getWidth());
                int y = (int)((p.getY() / height)*r.getHeight());
                if(r.getRGBA(x, y).getAlpha() == 0){
                    return true;
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return false;

    }
}
