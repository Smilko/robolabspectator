package at.htl.leonding.robolabcam.logic;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;
import at.htl.leonding.robolabcam.rest.Uploader;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ScreenshotController {
    private static ScreenshotController ourInstance = new ScreenshotController();

    public static ScreenshotController getInstance() {
        return ourInstance;
    }

    private int movementId = 100;
    private Date grouping = new Date();

    private ScreenshotController() {
    }

    public void incrementGrouping(){
        grouping = new Date();
    }


    public void saveScreenshot() {
        String path = null;
        String movementName = null;
        try {
            path = Properties.getProperty(Property.destinationRoot) + File.separator;

            SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            movementName = dt1.format(grouping);

            /*File file = new File(path + File.separator + movementName);
            if (!file.exists())
                file.mkdirs();*/

            BufferedImage bi = ImageProvider.getInstance().getImage();
            String filename = Properties.getProperty(Property.destinationGroupName) + movementId + "." + Properties.getProperty(Property.fileFormat);
            //File outputfile = new File(file.getAbsolutePath() + File.separator + filename);
            //System.out.println("Path: " + outputfile.getAbsolutePath());
            //ImageIO.write(bi, Properties.getProperty(Property.fileFormat), outputfile);

            Uploader.sendImage(bi, movementName + '/' + filename);

            //Sender.getInstance().sendFile(outputfile,movementName + pD + filename, true);
            //Sender.getInstance().sendFile(outputfile, filename, true);
            //outputfile.delete();
            //file.delete();
            movementId++;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
