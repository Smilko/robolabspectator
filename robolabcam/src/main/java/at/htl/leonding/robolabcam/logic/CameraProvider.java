package at.htl.leonding.robolabcam.logic;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamLock;
import com.github.sarxos.webcam.ds.ipcam.IpCamDevice;
import com.github.sarxos.webcam.ds.ipcam.IpCamDeviceRegistry;
import com.github.sarxos.webcam.ds.ipcam.IpCamDriver;
import com.github.sarxos.webcam.ds.ipcam.IpCamMode;

import java.net.MalformedURLException;

public class CameraProvider {

    private IpCamDevice device = new IpCamDevice("RaspberryPi", "http://localhost:8081", IpCamMode.PUSH);

    private CameraProvider() throws MalformedURLException {
        setup();
    }

    private void setup() throws MalformedURLException {
        Webcam.setDriver(new IpCamDriver());

        IpCamDeviceRegistry.register(device);

//        if(device.isOnline() && !device.isOpen()){
//            device.open();
//        }

        System.out.println(
                String.format("Device %s is %s and %s.",
                        device.getName(),
                        device.isOnline()?"online":"offline",
                        device.isOpen()?"open":"closed"));
    }

    public Webcam getWebcam(){
        for(Webcam cam : Webcam.getWebcams()){
            if(cam.getName().equals(device.getName())){
                if (!cam.isOpen()) {
                    cam.open();
                }
                return cam;
            }
        }
        return null;
    }


    private static CameraProvider ourInstance;

    public static CameraProvider getInstance() {
        if(ourInstance == null) {
            try {
                ourInstance = new CameraProvider();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return ourInstance;
    }
}
