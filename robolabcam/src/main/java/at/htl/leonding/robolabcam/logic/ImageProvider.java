package at.htl.leonding.robolabcam.logic;

import java.awt.image.BufferedImage;
import java.net.MalformedURLException;

public class ImageProvider {

    private static ImageProvider ourInstance = new ImageProvider();

    public static ImageProvider getInstance() {
        return ourInstance;
    }

    private ImageProvider() {
    }

    private void setup(){

    }

    public BufferedImage getImage() {
        return CameraProvider.getInstance().getWebcam().getImage();
    }
}
