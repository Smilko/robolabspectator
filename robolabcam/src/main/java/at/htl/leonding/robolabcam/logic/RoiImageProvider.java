package at.htl.leonding.robolabcam.logic;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class RoiImageProvider {

    private BufferedImage image;
    private LocalDateTime lastReadtime;
    private static RoiImageProvider instance = null;

    public static RoiImageProvider getInstance(){
        if(instance == null)
            instance = new RoiImageProvider();
        return instance;
    }

    private RoiImageProvider() {
        lastReadtime = LocalDateTime.MIN;
        loadImage();
    }

    private void loadImage(){
        try {
            image = ImageIO.read(new File(Properties.getProperty(Property.regionOfInteresFile)));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public Color getRGBA(int x, int y){
        LocalDateTime now = LocalDateTime.now();
        Color c = new Color(image.getRGB(x, y), true);
        return c;
    }

    public int getWidth(){
        return image.getWidth();
    }

    public int getHeight(){
        return image.getHeight();
    }

    //For Debugging
    /*public void saveImage(Mat image){
        BufferedImage bimg = getBufferdImageFromMat(image);
        File outputfile = new File("test.png");
        try {
            ImageIO.write(bimg, Properties.getProperty(Property.fileFormat), outputfile);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/
}
