package at.htl.leonding.robolabcam.info;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class InfoDat {

    public static void rewriteFileText(String newText) throws NoSuchFieldException, IOException {
        File f = new File(Properties.getProperty(Property.infoDatPath));

        if(f.exists())
            f.delete();
        f.createNewFile();
        FileOutputStream file = new FileOutputStream(f);
        file.write(newText.getBytes());
    }

    public static String getFileText() throws NoSuchFieldException, IOException {
        StringBuilder text = new StringBuilder();
        List<String> lines = Files.readAllLines(Paths.get(Properties.getProperty(Property.infoDatPath)));

        for (String line : lines) {
            text.append(line);
            text.append('\n');
        }

        return text.toString();
    }
}
