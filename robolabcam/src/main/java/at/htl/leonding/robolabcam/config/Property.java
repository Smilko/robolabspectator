package at.htl.leonding.robolabcam.config;

public class Property {
    public static final String  fileName = "config.properties",
            useMqttUpload = "useMqttUpload",
            useRestUpload = "useRestUpload",
            screenDuration = "screenDuration",
            movementRatio = "movementRatio",
            groupAfterTime = "groupAfterTime",
            destinationRoot = "destinationRoot",
            destinationGroupName = "destinationGroupName",
            movementFolderName = "movementFolderName",
            fileFormat = "fileFormat",
            ftpServerAdress = "ftpServerAdress",
            ftpPort = "ftpPort",
            ftpUser = "ftpUser",
            ftpPassword = "ftpPassword",
            regionOfInteresFile = "regionImageFileName",
            refreshImageSeconds = "refreshImageSeconds",
            serverURL = "serverURL",
            infoDatPath = "infoDatPath",
            mqttURL = "mqttURL",
            mqttChannel = "mqttChannel";
}
