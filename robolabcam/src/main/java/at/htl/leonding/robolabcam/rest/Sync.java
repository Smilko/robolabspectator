package at.htl.leonding.robolabcam.rest;

import at.htl.leonding.robolabcam.config.Properties;
import at.htl.leonding.robolabcam.config.Property;
import at.htl.leonding.robolabcam.info.InfoDat;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.apache.http.entity.mime.MIME.UTF8_CHARSET;

public class Sync {
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private static Sync instance = null;

    private boolean update = true;

    public boolean isUpdate() {
        return update;
    }

    public void setUpdate(boolean update) {
        this.update = update;
    }

    public static Sync getInstance() {
        if (instance == null)
            instance = new Sync();
        return instance;
    }

    public void init() {
        scheduler.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if(update){
                    System.out.println("--- Update configs ---");
                    updateConfigFileText();
                    updateInfoDatText();
                    updateRegionFile();
                }
            }
        }, 0, 5, TimeUnit.SECONDS);
    }

    private Sync() {
    }


    private void updateRegionFile() {
        try {
            HttpClient httpclient = HttpClientBuilder.create().build();
            String url = String.format("%s/%s", Properties.getProperty(Property.serverURL), "setting/screenArea");
            HttpGet getRequest = new HttpGet(url);

            HttpResponse response = httpclient.execute(getRequest);
            if (response.getStatusLine().getStatusCode() != 200)
                System.out.println("Get request executed with response: " + response.getStatusLine());
            else {
                InputStream is = response.getEntity().getContent();
                String filePath = Properties.getProperty(Property.regionOfInteresFile);
                FileOutputStream fos = new FileOutputStream(new File(filePath));
                int inByte;
                while ((inByte = is.read()) != -1)
                    fos.write(inByte);
                is.close();
                fos.close();
            }
        } catch (IOException | NoSuchFieldException ex) {
            ex.printStackTrace();
        }
    }

    private void updateConfigFileText() {
        try {
            String file = getTextFromServer("settingFile");
            Properties.rewriteConfigFile(file);
        } catch (NoSuchFieldException | IOException e) {
            e.printStackTrace();
        }
    }

    private void updateInfoDatText() {
        try {
            String text = getTextFromServer("infoDat");
            InfoDat.rewriteFileText(text);
        } catch (NoSuchFieldException | IOException e) {
            e.printStackTrace();
        }
    }

    private String getTextFromServer(String path) throws NoSuchFieldException, IOException {
        HttpClient httpclient = HttpClientBuilder.create().build();
        String url = String.format("%s/%s", Properties.getProperty(Property.serverURL), path);
        HttpGet getRequest = new HttpGet(url);

        HttpResponse response = httpclient.execute(getRequest);
        if (response.getStatusLine().getStatusCode() != 200)
            System.out.println("Get request executed with response: " + response.getStatusLine());

        return EntityUtils.toString(response.getEntity(), UTF8_CHARSET).toString();
    }
}
