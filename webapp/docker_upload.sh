scp -r app/ schloemi@vm81.htl-leonding.ac.at:/opt/robolabcam/webapp

scp -r test/ schloemi@vm81.htl-leonding.ac.at:/opt/robolabcam/webapp

scp .* schloemi@vm81.htl-leonding.ac.at:/opt/robolabcam/webapp

scp gulpfile.js schloemi@vm81.htl-leonding.ac.at:/opt/robolabcam/webapp

scp package*.json schloemi@vm81.htl-leonding.ac.at:/opt/robolabcam/webapp

scp docker-compose.yml schloemi@vm81.htl-leonding.ac.at:/opt/robolabcam/webapp

