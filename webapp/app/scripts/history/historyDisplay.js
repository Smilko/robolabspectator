console.log('HistoryDisplay loaded');


const CONTAINER_NAME = 'movementHistoryContainer';
const CONTAINER_NAME_LATEST_HISTORY = 'latestHistoryContainer';
const FULLSCREEN_CHECKBOX = 'movementHistoryFullscreenMode';
const INPUT_DATE_FROM = 'inputDateFrom';
const INPUT_DATE_TO = 'inputDateTo';
const BTN_FILTER = 'dateFilter';

class MovmementHistoryController {
  constructor() {

  }

  /*
  Create a new Card for a movement
  */
  createNewMovementCard(container, rowDetail, imageUrl, title, idx, data, isSmall) {
    let child = document.createElement('div');
    child.id = title;

    if (isSmall) {
      child.classList.add('col-sm-4');
    }
    else {
      child.classList.add('col-sm-6');
    }

    let card = document.createElement('div');
    card.classList.add('card');
    card.classList.add('m-1');

    let cardBody = document.createElement('div');
    cardBody.classList.add('card-body');


    let cardImage = document.createElement('img');
    cardImage.classList.add('card-img-top');
    cardImage.classList.add('text-danger');
    cardImage.setAttribute('src', imageUrl);
    cardImage.onclick = ev => {
      window.location.href = 'movementHistory.html#' + title
    };

    let cardTitle = document.createElement('h4');
    cardTitle.classList.add('card-title');


    let titleDate = document.createElement('label');
    titleDate.classList.add('badge');
    titleDate.classList.add('badge-secondary');
    titleDate.classList.add('mx-1');
    titleDate.appendChild(document.createTextNode(new Date(title).toLocaleDateString()));
    cardTitle.appendChild(titleDate);

    let titleTime = document.createElement('label');
    titleTime.classList.add('badge');
    titleTime.classList.add('badge-primary');
    titleTime.classList.add('mx-1');
    titleTime.appendChild(document.createTextNode(new Date(title).toLocaleTimeString()));
    cardTitle.appendChild(titleTime);

    let titleCnt = document.createElement('label');
    titleCnt.classList.add('float-right');
    titleCnt.appendChild(document.createTextNode(`${data[idx].images.length} Bilder`));
    cardTitle.appendChild(titleCnt);

    let cardButton = document.createElement('a');
    cardButton.id = title + '_sliderButton';
    cardButton.classList.add('btn');
    cardButton.classList.add('btn-block');
    cardButton.classList.add('btn-dark');
    cardButton.classList.add('text-light');
    cardButton.appendChild(document.createTextNode('View'));
    cardButton.onclick = function () {
      if (rowDetail.querySelector('#slider' + idx) === null) {
        rowDetail.innerHTML = '';
        rowDetail.parentNode.classList.remove('invisible');

        if (document.getElementById(FULLSCREEN_CHECKBOX).classList.contains('active')) {
          rowDetail.parentNode.style.position = 'fixed';
          rowDetail.parentNode.style.top = '0';
          rowDetail.parentNode.style.left = '0';
          rowDetail.parentNode.style.zIndex = '100';
          rowDetail.parentNode.style.bottom = '0';

          rowDetail.classList.remove('my-2');
        }
        else if (false === document.getElementById(FULLSCREEN_CHECKBOX).classList.contains('active')) {
          console.log('fullscreen not active');
          rowDetail.parentNode.style.position = '';
          rowDetail.parentNode.style.top = '';
          rowDetail.parentNode.style.left = '';
          rowDetail.parentNode.style.zIndex = '';

          rowDetail.classList.add('my-2');
        }


        let closeButton = document.createElement('div');
        closeButton.innerHTML = '<button type="button" class="close" aria-label="Close">' +
          '  <span aria-hidden="true">&times;</span>' +
          '</button>';
        closeButton.onclick = function () {
          cardButton.click();
        }

        rowDetail.appendChild(closeButton);

        mhc.createSlider(rowDetail, data, idx);

        // card.removeChild(cardImage);
      } else {
        // card.insertBefore(cardImage, card.firstChild);
        rowDetail.parentNode.classList.add('invisible');
        rowDetail.innerHTML = '';
      }
    };

    cardBody.appendChild(cardTitle);
    if (!isSmall)
      cardBody.appendChild(cardButton);

    card.appendChild(cardImage);
    card.appendChild(cardBody);
    child.appendChild(card);

    container.appendChild(child);
  }

  createAllMovementCards(movements, data) {
    let rootContainer = document.getElementById(CONTAINER_NAME);

    let showLatestHistory = false;

    if (rootContainer == null) {
      rootContainer = document.getElementById(CONTAINER_NAME_LATEST_HISTORY);
      showLatestHistory = true;
    }

    rootContainer.innerHTML = '';

    let container = document.createElement('div');
    container.classList.add('col-sm-12');

    rootContainer.appendChild(container);

    if (data.length === 0) {
      container.innerHTML = '<br><div class="alert alert-secondary" role="alert">' +
        '  <h1 class="display-1">( •᷄⌓•᷅ ) No items found.</h1>' +
        '</div>';
    } else {
      if (!showLatestHistory) {
        movements.forEach((m, idx) => {
          if ((idx % 2) === 1) {
            this.createMovementCarRow(container, data, movements[idx - 1], movements[idx], idx - 1, idx);
          }
          else if (idx === movements.length - 1) { //odd idx + last row
            this.createMovementCarRow(container, data, movements[idx], null, idx);
          }
        });

        let focusedElement = document.getElementById(location.hash.substr(1)+'_sliderButton');
        if(focusedElement != null){
          focusedElement.click();
        }
      }
      else {
        this.createMovementCar3PartRow(container, data, movements[0], movements[1], movements[2], 0, 1, 2);
      }
    }
  }

  createMovementCarRow(container, data, movement1, movement2, idx1, idx2) {
    let curRow = document.createElement('div');
    curRow.classList.add('row');

    let curRowInner = document.createElement('div');
    curRowInner.classList.add('row');

    let curRowDetailContainer = document.createElement('div');
    curRowDetailContainer.classList.add('col-sm-12');
    curRowDetailContainer.classList.add('card');
    curRowDetailContainer.classList.add('bg-warning');
    curRowDetailContainer.classList.add('invisible');

    let curRowDetail = document.createElement('div');
    curRowDetail.classList.add('col-sm-12');

    this.createNewMovementCard(curRowInner, curRowDetail, movement1.url, movement1.title, idx1, data);
    if (movement2 != null) {
      this.createNewMovementCard(curRowInner, curRowDetail, movement2.url, movement2.title, idx2, data);
    }

    curRow.appendChild(curRowInner);
    curRow.appendChild(curRowDetailContainer);
    curRowDetailContainer.appendChild(curRowDetail);
    container.appendChild(curRow);
  }

  fetchMovements(callback) {
    let filterQuery = '';

    if (document.getElementById(INPUT_DATE_FROM) !== null &&
      document.getElementById(INPUT_DATE_TO)) {
      let dateFrom = new Date(document.getElementById(INPUT_DATE_FROM).value);
      let dateTo = new Date(document.getElementById(INPUT_DATE_TO).value);

      if (!isNaN(dateFrom) && !isNaN(dateTo)) {
        filterQuery = `?from=${dateFrom.toISOString().split('T')[0]}&to=${dateTo.toISOString().split('T')[0]}`;
      }
    }

    fetch(restDomain + '/history/' + filterQuery)
      .then((resp) => {
        return resp.json();
      }).then((data) => {
      this.data = data;
      callback(data);
    });
  }

  createSlider(container, data, idx) {
    let slider = document.createElement('div');
    slider.id = `slider${idx}`;
    slider.classList.add('swiper-container');
    slider.classList.add('bg-dark');
    slider.classList.add(`sld${idx}`);

    //Previous Button
    let sliderPrevButton = document.createElement('div');
    sliderPrevButton.classList.add('swiper-button-prev');
    slider.appendChild(sliderPrevButton);

    //Next Button
    let sliderNextButton = document.createElement('div');
    sliderNextButton.classList.add('swiper-button-next');
    slider.appendChild(sliderNextButton);

    //Next Button
    let sliderScrollbar = document.createElement('div');
    sliderScrollbar.classList.add('swiper-scrollbar');
    slider.appendChild(sliderScrollbar);

    //Slide Wrapper
    let sliderWrapper = document.createElement('div');
    sliderWrapper.classList.add('swiper-wrapper');
    slider.appendChild(sliderWrapper);

    //Images
    data[idx].images.forEach(item => {
      let url = restDomain + '/image/' + data[idx].folder + '/' + item;
      let slide = document.createElement('div');
      slide.classList.add('swiper-slide');

      let imageElement = document.createElement('img');
      imageElement.setAttribute('src', url);
      imageElement.style.width = '100%';
      slide.appendChild(imageElement);

      sliderWrapper.appendChild(slide);
    });

    container.appendChild(slider);
    this.createSliderObject(`.sld${idx}`);

    setTimeout(function () {
      slider.scrollIntoView(true);
      window.scrollTo(0, window.pageYOffset-50);
    },250);
  }

  createSliderObject(id) {
    var mySwiper = new Swiper(id, {
      // Optional parameters
      direction: 'horizontal',
      loop: true,
      keyboard: true,

      // Navigation arrows
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },

      scrollbar: {
        el: '.swiper-scrollbar',
        draggable: true,
      },

      autoplay: {
        delay: 2000,
      },
    });
  }

  createMovementCar3PartRow(container, data, movement1, movement2, movement3, idx1, idx2, idx3) {
    let curRow = document.createElement('div');
    curRow.classList.add('row');

    let curRowInner = document.createElement('div');
    curRowInner.classList.add('row');

    let curRowDetail = document.createElement('div');
    curRowDetail.classList.add('col-sm-12');

    if (movement1 != null) {
      this.createNewMovementCard(curRowInner, curRowDetail, movement1.url, movement1.title, idx1, data, true);
    }
    if (movement2 != null) {
      this.createNewMovementCard(curRowInner, curRowDetail, movement2.url, movement2.title, idx2, data, true);
    }
    if (movement3 != null) {
      this.createNewMovementCard(curRowInner, curRowDetail, movement3.url, movement3.title, idx3, data, true);
    }

    curRow.appendChild(curRowInner);
    container.appendChild(curRow);
  }
}

var mhc = new MovmementHistoryController();

mhc.fetchMovements(function (data) {
  let movementCardData = [];
  data.forEach(item => {
    movementCardData.push(({
      title: item.folder,
      url: restDomain + '/image/' + item.folder + '/' + item.images[0]
    }));
  });
  mhc.createAllMovementCards(movementCardData, data);
});

let filterSubmit = document.getElementById(BTN_FILTER);

if (filterSubmit != null) {
  filterSubmit.onclick = function () {
    mhc.fetchMovements(function (data) {
      let movementCardData = [];
      data.forEach(item => {
        movementCardData.push(({
          title: item.folder,
          url: restDomain + '/image/' + item.folder + '/' + item.images[0]
        }));
      });
      mhc.createAllMovementCards(movementCardData, data);
    });
  }
}


Date.prototype.yyyymmdd = function () {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
    (mm > 9 ? '' : '0') + mm,
    (dd > 9 ? '' : '0') + dd
  ].join('-');
};

if (document.getElementById(INPUT_DATE_TO))
  document.getElementById(INPUT_DATE_TO).value = new Date().yyyymmdd();
let dateToInit = new Date(new Date().getTime() - 1000 * 60 * 60 * 24 * 7);

if (document.getElementById(INPUT_DATE_FROM))
  document.getElementById(INPUT_DATE_FROM).value = dateToInit.yyyymmdd();

//movementHistory click on image of gallery
window.onhashchange = function (ev){
  let focusedElement = document.getElementById(location.hash.substr(1)+'_sliderButton');
  if(focusedElement != null){
    focusedElement.click();
  }
};
