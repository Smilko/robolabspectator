console.log('Robolabcam Webapp @HTL Leonding');

let restIP = 'vm81.htl-leonding.ac.at';
let restPort = 8100;

let restDomain = 'http://' + restIP + ':' + restPort;
let streamDomain = restDomain + '/cam';

function getData(url, callback) {
  fetch(restDomain + url)
    .then((response) => {
      return response.json();
    }).then((data) => {
    callback(data);
  });
}
