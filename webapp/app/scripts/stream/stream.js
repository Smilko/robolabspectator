console.log('Stream JS loaded');


let streamContainer = document.getElementById('stream_container');
let streamImage = document.getElementById('stream_image');

streamContainer.classList.add('d-flex');
streamContainer.classList.add('justify-content-center');
streamContainer.classList.add('align-items-center');
streamContainer.classList.add('w-100');

streamImage.classList.add('w-100');

streamImage.onerror = function (er) {
  console.log('Error loading Stream');

  //TODO Show something cool on userscreen
  streamImage.alt = '';

  let streamUnavailable = document.createElement('div');

  streamUnavailable.innerHTML = '<div class="alert alert-warning" role="alert">' +
    '  <h1 class="display-4">ヽ(ﾟДﾟ)ﾉ Stream unavailable</h1>' +
    '</div>';

  let streamParent = streamContainer;
  streamParent.style.textDecoration = 'none'; //link
  streamParent.href = '#'; //link
  streamParent.style.cursor = 'default';

  streamParent.removeChild(streamImage);
  streamParent.appendChild(streamUnavailable);


  streamUnavailable.classList.add('stream-error');
};

streamImage.onload = function () {
  console.log('Stream established');
};

streamImage.src = streamDomain;


if (streamImage.classList.contains('no-fullscreen')) {
  streamContainer.style.cursor = 'default';
}

streamContainer.onclick = function (ev) {
  if (streamImage.classList.contains('no-fullscreen') === false &&
    streamImage.classList.contains('stream-error') === false) {
    toggleFullScreen();
  }
};

function toggleFullScreen() {
  if (!document.fullscreenElement &&
    !document.msFullscreenElement &&
    !document.mozFullScreenElement &&
    !document.webkitFullscreenElement) {
    if (streamImage.requestFullscreen) {
      streamContainer.requestFullscreen();
    } else if (streamContainer.msRequestFullscreen) {
      streamContainer.msRequestFullscreen();
    } else if (streamContainer.mozRequestFullScreen) {
      streamContainer.mozRequestFullScreen();
    } else if (streamContainer.webkitRequestFullscreen) {
      streamContainer.webkitRequestFullscreen();
    }
  } else {
    if (document.exitFullscreen) {
      document.exitFullscreen();
    } else if (document.msExitFullscreen) {
      document.msExitFullscreen();
    } else if (document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if (document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}

document.onfullscreenchange = function () {
  if (!document.fullscreenElement &&
    !document.msFullscreenElement &&
    !document.mozFullScreenElement &&
    !document.webkitFullscreenElement) {
    streamImage.classList.add('card-img-top');
    // streamContainer.classList.add('d-flex');
    // streamContainer.classList.add('justify-content-center');
  }
  else {
    streamImage.classList.remove('card-img-top');
    // streamContainer.classList.remove('d-flex');
    // streamContainer.classList.remove('justify-content-center');
  }
};

document.onwebkitfullscreenchange = document.onfullscreenchange;
document.onmozfullscreenchange = document.onfullscreenchange;

document.onkeydown = function (e) {
  if (e.keyCode === 13 && //ENTER Key
    streamImage.classList.contains('no-fullscreen') === false &&
    streamImage.classList.contains('stream-error') === false) {
    toggleFullScreen();
  }
};

function checkForText() {
  console.log('Check for Stream Text');
  fetch(restDomain + '/text')
    .then((response) => {
      return response.text();
    }).then((data) => {
    let node = document.createElement('div');
    node.innerHTML = data;
    node.classList.add('stream-text');
    node.style.position = 'absolute';

    let oldItems = document.getElementsByClassName('stream-text');
    for (let i=0;i<oldItems.length;i++){
      if(oldItems[i].classList.contains('stream-text')){
        streamContainer.removeChild(oldItems[i]);
      }
    }
    streamContainer.appendChild(node);
  });
}

// INFO Text shown as HTML
// setInterval(checkForText, 1000);
