var liveImage = document.getElementById('liveImage');
var editCanvas = document.getElementById('editCanvas');
var showCanvas = document.getElementById('showCanvas');
var mouseCanvas = document.getElementById('mouseCanvas');
var colorInput = document.getElementById('colorPicker');
var mouseColorInput = document.getElementById('mouseColorPicker');
var thickness = document.getElementById('thickness');
var btnClearAll = document.getElementById('btnClearAll');
var tool = -1;
var shouldUseTool = false;
var isMouseDown = false;

window.onload = init;
function init(){
    window.onmouseup = mouseUp;
    window.onmouseout = mouseUp;
    window.onmousedown = mouseDown;
    window.onmousemove = drawMouseCanvas;
    window.setInterval(updateShowCanvas, 1000/60);
    showCanvas.onmousedown = useToolAndDraw;
    showCanvas.onmouseup = stopTool;
    showCanvas.onmouseout = stopTool;
    showCanvas.onmousemove = draw;
    document.getElementById('pencilRadio').onclick = usePencil;
    document.getElementById('eraserRadio').onclick = useEraser;
    document.getElementById('saveImageBtn').onclick = uploadImage;
    tool = 1;
    thickness.onchange = updateTexts;
    btnClearAll.onclick = clearAll;

    updateTexts();
    showCanvas.width = editCanvas.width;
    showCanvas.height = editCanvas.height;
    mouseCanvas.width = editCanvas.width;
    mouseCanvas.height = editCanvas.height;

    document.getElementById('liveImage').src = streamDomain;
    downloadCurrentImage();
}

function uploadImage(){
    var image = editCanvas.toDataURL('image/png');
    image = image.replace('data:image/png;base64,', '');

    $.ajax({
        type: 'POST',
        url: restDomain + '/setting/screenArea',
        data: '{ "imageData" : "' + image + '" }',
        contentType: 'application/json; charset=utf-8',
        success: function(msg) {
            alert('Bild erfolgreich hochgeladen!');
        },
        error: function(err){
            alert('Fehler beim hochladen der Datei:\n' + err);
        }
    });
}

function downloadCurrentImage() {
  let context = editCanvas.getContext('2d');
  let img = new Image();
  img.onload = function(){
    context.drawImage(img, 0, 0, showCanvas.width, showCanvas.height);
  };
  img.src = restDomain + '/setting/screenArea';
  img.crossOrigin = "Anonymous";
}

function updateShowCanvas(){
    var context = showCanvas.getContext('2d');
    context.drawImage(liveImage, 0, 0, showCanvas.width, showCanvas.height);
    context.drawImage(editCanvas, 0, 0, showCanvas.width, showCanvas.height);
    context.drawImage(mouseCanvas, 0, 0, showCanvas.width, showCanvas.height);
}

function updateTexts(){
    document.getElementById('thicknessText').innerHTML = thickness.value;
}

function drawMouseCanvas(){
    var mContext = mouseCanvas.getContext('2d');
    var coord = getMCord();

    mContext.beginPath();
    mContext.lineWidth = 3;
    mContext.clearRect(0, 0, mouseCanvas.width, mouseCanvas.height);
    mContext.strokeStyle = mouseColorInput.value;
    mContext.arc(coord.x, coord.y, thickness.value, 0, 2 * Math.PI);
    mContext.stroke();
}

function draw(event){
    if (!isMouseDown){
        return;
    }
    if (tool != 0 && tool != 1) {
        return;
    }
    drawForeground(event);
}

function drawForeground(event){
    var coord = getMCord();
    var context = editCanvas.getContext('2d');
    context.beginPath();
    if (shouldUseTool) {
        if (tool === 1) {
            context.fillStyle = colorInput.value;
            context.arc(coord.x, coord.y, thickness.value, 0, 2 * Math.PI);
            context.fill();
        }
        if (tool === 0) {
            context.fillStyle = colorInput.value;
            context.save();
            context.arc(coord.x, coord.y, thickness.value, 0, 2 * Math.PI);
            context.clip();
            context.clearRect(coord.x - thickness.value, coord.y - thickness.value, thickness.value*2, thickness.value*2);
            context.restore();
        }
    }
}

function getMCord(){
    var offset = $('#showCanvas').offset();

    var X = event.pageX - offset.left;
    var Y = event.pageY - offset.top;
    var width = showCanvas.width;
    var cWidth = showCanvas.clientWidth;
    var height = showCanvas.height;
    var cHeight = showCanvas.clientHeight;

    X = (X/cWidth)*width;
    Y = (Y/cHeight)*height;

    return {x: X, y: Y};
}

function clearAll(){
    var context = editCanvas.getContext('2d');
    context.clearRect(0,0, editCanvas.width, editCanvas.height);
}

function useToolAndDraw(event){
    useTool();
    mouseDown();
    draw(event);
}

function stopTool(){
    shouldUseTool = false;
}

function useTool(){
    shouldUseTool = true;
}

function mouseDown(){
    isMouseDown = true;
}

function mouseUp(){
    isMouseDown = false;
}

function useEraser(){
    tool = 0;
}

function usePencil(){
    tool = 1;
}

