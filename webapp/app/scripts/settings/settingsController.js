var intervallInput = document.getElementById('movementIntervall');
var movementRatioInput = document.getElementById('movementRatio');
var groupAfterTimeInput = document.getElementById('groupAfterTime');

var titleMovementText = 'In welchen Zeitabständen sollen die Fotos aufgenommen werden, wenn eine Bewegung erkannt wurde?';
var titleMovementRatioText = 'Wie stark muss sich etwas im Bild bewegt haben, um Fotos aufzunehmen?';
var titleGroupTimeOutText = 'Nach wievielen Sekunden soll eine neue Grupierung der Fotos angefangen werden?';

init();
function init(){
  intervallInput.onchange = updatePropertyIntervall;
  groupAfterTimeInput.onchange = updatePropertyGroupAfter;
  movementRatioInput.onchange = updatePropertyMovementRatio;
  getData('/setting/screenDuration', function (data){ intervallInput.value = data; showValues();});
  getData('/setting/movementRatio', function (data){ movementRatioInput.value = data; showValues();});
  getData('/setting/groupAfterTime', function (data){ groupAfterTimeInput.value = data; showValues();});
  showValues();
  loadText();
}

function updateProperty(url){
  console.log('Updating ' + restDomain + url);
  showValues();
  fetch(restDomain + url, {method: 'PUT'}).then((resp) => { console.log(resp.status)});
  showSnackbar();
}

function updatePropertyIntervall(){
  updateProperty('/setting/screenDuration/' + intervallInput.value);
}

function updatePropertyMovementRatio(){
  updateProperty('/setting/movementRatio/' + movementRatioInput.value);
}

function updatePropertyGroupAfter(){
  updateProperty('/setting/groupAfterTime/' + groupAfterTimeInput.value);
}

function showValues(){
  document.getElementById('intervallText').innerHTML = intervallInput.value;
  document.getElementById('movenentRatioText').innerHTML = movementRatioInput.value;
}

function showSnackbar() {
  var x = document.getElementById('snackbar');
  x.className = 'show';
  setTimeout(function(){ x.className = x.className.replace('show', ''); }, 3000);
}

function loadText(){
  console.log('Loging');
  document.getElementById('titleMovement').title = titleMovementText;
  document.getElementById('titleMovementRatio').title = titleMovementRatioText;
  document.getElementById('titleGroupTimeOut').title = titleGroupTimeOutText;
}
